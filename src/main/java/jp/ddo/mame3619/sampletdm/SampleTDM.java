package jp.ddo.mame3619.sampletdm;

import jp.ddo.mame3619.sampletdm.commands.StageCommands;
import jp.ddo.mame3619.sampletdm.listener.PlayerListener;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.permissions.Permissible;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.*;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.Set;

public class SampleTDM extends JavaPlugin{

    @Override
    public void onEnable() {
        registerCommands();
        registerListener();


    }

    @Override
    public void onDisable() {


    }

    public void registerListener() {
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new PlayerListener(), this);




    }

    public void loadClass(){


    }


    public void registerCommands(){
        getCommand("stage").setExecutor(new StageCommands());
    //    getCommand("dev").setExecutor(new DeveloperCommand());
    }



}
